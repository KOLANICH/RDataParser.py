# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
from kaitaistruct import __version__ as ks_version, KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if parse_version(ks_version) < parse_version('0.7'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.7 or later is required, but you have %s" % (ks_version))

class RData(KaitaiStruct):
    """A set of file formats used for serialization of objects of [R language](https://www.r-project.org/about.html) for statistical computing.
    Datasets are often distributed in these formats.
    RDA files are usually compressed, you need to detect compression and decompress them before applying the code generated from this spec.
    There are 2 different formats, "old" and "new", the version is set up via is_new parameter.
    
    .. seealso::
       Source - https://github.com/wch/r-source/blob/master/src/main/saveload.c
       https://github.com/wch/r-source/blob/master/src/include/Rinternals.h
       https://github.com/wch/r-source/blob/master/src/main/serialize.c
       https://cran.r-project.org/doc/manuals/r-release/R-ints.html#Serialization-Formats
    """
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.signature = self._io.ensure_fixed_contents(b"\x52\x44\x58\x32\x0A")
        self.signature_type = self._io.read_bytes(1)
        self.lf = self._io.ensure_fixed_contents(b"\x0A")
        self.body = self._root.Body(self._io, self, self._root)

    class Body(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            _on = self._root.signature_type
            if _on == b"\x42":
                self._is_le = True
            elif _on == b"\x58":
                self._is_le = False

            if self._is_le == True:
                self._read_le()
            elif self._is_le == False:
                self._read_be()
            else:
                raise Exception("Unable to decide endianness")

        def _read_le(self):
            self.version = self._root.Body.Version(self._io, self, self._root, self._is_le)
            if self._root.is_new == 0:
                self.writer_version = self._root.Body.Version(self._io, self, self._root, self._is_le)

            if self._root.is_new == 0:
                self.release_version = self._root.Body.Version(self._io, self, self._root, self._is_le)

            if self._root.is_new == 1:
                self.sym_count = self._io.read_s4le()

            if self._root.is_new == 1:
                self.env_count = self._io.read_s4le()

            self.ref_table = self._root.Body.Item(self._io, self, self._root, self._is_le)
            if self._root.is_new == 1:
                self.symbols = [None] * (self.sym_count)
                for i in range(self.sym_count):
                    self.symbols[i] = self._root.Body.PasStr(self._io, self, self._root, self._is_le)


            if self._root.is_new == 1:
                self.env = [None] * (self.env_count)
                for i in range(self.env_count):
                    self.env[i] = self._root.Body.EnvItem(self._io, self, self._root, self._is_le)


            if self._root.is_new == 1:
                self.obj = self._root.Body.Item(self._io, self, self._root, self._is_le)


        def _read_be(self):
            self.version = self._root.Body.Version(self._io, self, self._root, self._is_le)
            if self._root.is_new == 0:
                self.writer_version = self._root.Body.Version(self._io, self, self._root, self._is_le)

            if self._root.is_new == 0:
                self.release_version = self._root.Body.Version(self._io, self, self._root, self._is_le)

            if self._root.is_new == 1:
                self.sym_count = self._io.read_s4be()

            if self._root.is_new == 1:
                self.env_count = self._io.read_s4be()

            self.ref_table = self._root.Body.Item(self._io, self, self._root, self._is_le)
            if self._root.is_new == 1:
                self.symbols = [None] * (self.sym_count)
                for i in range(self.sym_count):
                    self.symbols[i] = self._root.Body.PasStr(self._io, self, self._root, self._is_le)


            if self._root.is_new == 1:
                self.env = [None] * (self.env_count)
                for i in range(self.env_count):
                    self.env[i] = self._root.Body.EnvItem(self._io, self, self._root, self._is_le)


            if self._root.is_new == 1:
                self.obj = self._root.Body.Item(self._io, self, self._root, self._is_le)


        class EnvItem(KaitaiStruct):
            def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                self._io = _io
                self._parent = _parent
                self._root = _root if _root else self
                self._is_le = _is_le
                self._read()

            def _read(self):

                if self._is_le == True:
                    self._read_le()
                elif self._is_le == False:
                    self._read_be()
                else:
                    raise Exception("Unable to decide endianness")

            def _read_le(self):
                self.enclos = self._root.Body.Item(self._io, self, self._root, self._is_le)
                self.frame = self._root.Body.Item(self._io, self, self._root, self._is_le)
                self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)

            def _read_be(self):
                self.enclos = self._root.Body.Item(self._io, self, self._root, self._is_le)
                self.frame = self._root.Body.Item(self._io, self, self._root, self._is_le)
                self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)


        class PasStr(KaitaiStruct):
            def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                self._io = _io
                self._parent = _parent
                self._root = _root if _root else self
                self._is_le = _is_le
                self._read()

            def _read(self):

                if self._is_le == True:
                    self._read_le()
                elif self._is_le == False:
                    self._read_be()
                else:
                    raise Exception("Unable to decide endianness")

            def _read_le(self):
                self.len = self._io.read_s4le()
                self.str = (self._io.read_bytes(self.len)).decode(u"utf-8")

            def _read_be(self):
                self.len = self._io.read_s4be()
                self.str = (self._io.read_bytes(self.len)).decode(u"utf-8")


        class Item(KaitaiStruct):

            class Type(Enum):
                nilsxp = 0
                symsxp = 1
                listsxp = 2
                closxp = 3
                envsxp = 4
                promsxp = 5
                langsxp = 6
                specialsxp = 7
                builtinsxp = 8
                charsxp = 9
                lglsxp = 10
                intsxp = 13
                realsxp = 14
                cplxsxp = 15
                strsxp = 16
                dotsxp = 17
                anysxp = 18
                vecsxp = 19
                exprsxp = 20
                bcodesxp = 21
                extptrsxp = 22
                weakrefsxp = 23
                rawsxp = 24
                s4sxp = 25
                newsxp = 30
                freesxp = 31
                funsxp = 99
                altrep_sxp = 238
                attrlistsxp = 239
                attrlangsxp = 240
                baseenv_sxp = 241
                emptyenv_sxp = 242
                bcrepref = 243
                bcrepdef = 244
                persistsxp = 247
                packagesxp = 248
                namespacesxp = 249
                basenamespace_sxp = 250
                missingarg_sxp = 251
                unboundvalue_sxp = 252
                globalenv_sxp = 253
                nilvalue_sxp = 254
                refsxp = 255
            def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                self._io = _io
                self._parent = _parent
                self._root = _root if _root else self
                self._is_le = _is_le
                self._read()

            def _read(self):

                if self._is_le == True:
                    self._read_le()
                elif self._is_le == False:
                    self._read_be()
                else:
                    raise Exception("Unable to decide endianness")

            def _read_le(self):
                self.flags = self._root.Body.Item.Flags(self._io, self, self._root, self._is_le)
                _on = self.flags.type
                if _on == self._root.Body.Item.Type.builtinsxp:
                    self.contents = self._root.Body.Item.Specialsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.specialsxp:
                    self.contents = self._root.Body.Item.Specialsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.promsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.charsxp:
                    self.contents = self._root.Body.Item.CharVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.langsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.lglsxp:
                    self.contents = self._root.Body.Item.IntVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.altrep_sxp:
                    self.contents = self._root.Body.Item.AltrepSxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.refsxp:
                    self.contents = self._root.Body.Item.RefIndex(self.flags.ref_idx, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.namespacesxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.intsxp:
                    self.contents = self._root.Body.Item.IntVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.vecsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.dotsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.listsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.strsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.symsxp:
                    self.contents = self._root.Body.Item.Symsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.rawsxp:
                    self.contents = self._root.Body.Item.Raw(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.cplxsxp:
                    self.contents = self._root.Body.Item.ComplexVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.closxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.packagesxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.envsxp:
                    self.contents = self._root.Body.Item.Envsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.bcodesxp:
                    self.contents = self._root.Body.Item.Bytecode(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.extptrsxp:
                    self.contents = self._root.Body.Item.Extptrsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.persistsxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.realsxp:
                    self.contents = self._root.Body.Item.RealVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.exprsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)

            def _read_be(self):
                self.flags = self._root.Body.Item.Flags(self._io, self, self._root, self._is_le)
                _on = self.flags.type
                if _on == self._root.Body.Item.Type.builtinsxp:
                    self.contents = self._root.Body.Item.Specialsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.specialsxp:
                    self.contents = self._root.Body.Item.Specialsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.promsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.charsxp:
                    self.contents = self._root.Body.Item.CharVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.langsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.lglsxp:
                    self.contents = self._root.Body.Item.IntVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.altrep_sxp:
                    self.contents = self._root.Body.Item.AltrepSxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.refsxp:
                    self.contents = self._root.Body.Item.RefIndex(self.flags.ref_idx, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.namespacesxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.intsxp:
                    self.contents = self._root.Body.Item.IntVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.vecsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.dotsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.listsxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.strsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.symsxp:
                    self.contents = self._root.Body.Item.Symsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.rawsxp:
                    self.contents = self._root.Body.Item.Raw(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.cplxsxp:
                    self.contents = self._root.Body.Item.ComplexVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.closxp:
                    self.contents = self._root.Body.Item.DottedPair(self.flags, self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.packagesxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.envsxp:
                    self.contents = self._root.Body.Item.Envsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.bcodesxp:
                    self.contents = self._root.Body.Item.Bytecode(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.extptrsxp:
                    self.contents = self._root.Body.Item.Extptrsxp(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.persistsxp:
                    self.contents = self._root.Body.Item.StringVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.realsxp:
                    self.contents = self._root.Body.Item.RealVec(self._io, self, self._root, self._is_le)
                elif _on == self._root.Body.Item.Type.exprsxp:
                    self.contents = self._root.Body.Item.ItemsVec(self._io, self, self._root, self._is_le)

            class Envsxp(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    _on = self._root.is_new
                    if _on == 0:
                        self.envsxp = self._root.Body.Item.Envsxp.Old(self._io, self, self._root, self._is_le)
                    elif _on == 1:
                        self.envsxp = self._root.Body.Item.Envsxp.New(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    _on = self._root.is_new
                    if _on == 0:
                        self.envsxp = self._root.Body.Item.Envsxp.Old(self._io, self, self._root, self._is_le)
                    elif _on == 1:
                        self.envsxp = self._root.Body.Item.Envsxp.New(self._io, self, self._root, self._is_le)

                class Old(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.locked = self._io.read_s4le()
                        self.enclos = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.frame = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.hash_table = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.attrib = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    def _read_be(self):
                        self.locked = self._io.read_s4be()
                        self.enclos = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.frame = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.hash_table = self._root.Body.Item(self._io, self, self._root, self._is_le)
                        self.attrib = self._root.Body.Item(self._io, self, self._root, self._is_le)


                class New(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.pos = self._io.read_u4le()

                    def _read_be(self):
                        self.pos = self._io.read_u4be()



            class ItemsVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._root.Body.Item(self._io, self, self._root, self._is_le)


                def _read_be(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._root.Body.Item(self._io, self, self._root, self._is_le)



            class StringVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.name_len = self._io.read_s4le()
                    self.name = (self._io.read_bytes(self.name_len)).decode(u"utf-8")
                    self.len = self._io.read_s4le()
                    self.items = self._root.Body.Item(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    self.name_len = self._io.read_s4be()
                    self.name = (self._io.read_bytes(self.name_len)).decode(u"utf-8")
                    self.len = self._io.read_s4be()
                    self.items = self._root.Body.Item(self._io, self, self._root, self._is_le)


            class Flags(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    if self._root.is_new == 0:
                        self.flagz_old = self._root.Body.Item.Flags.Old(self._io, self, self._root, self._is_le)

                    if self._root.is_new == 1:
                        self.flagz_new = self._root.Body.Item.Flags.New(self._io, self, self._root, self._is_le)


                def _read_be(self):
                    if self._root.is_new == 0:
                        self.flagz_old = self._root.Body.Item.Flags.Old(self._io, self, self._root, self._is_le)

                    if self._root.is_new == 1:
                        self.flagz_new = self._root.Body.Item.Flags.New(self._io, self, self._root, self._is_le)


                class Old(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.flags = self._io.read_u4le()

                    def _read_be(self):
                        self.flags = self._io.read_u4be()

                    @property
                    def levels(self):
                        if hasattr(self, '_m_levels'):
                            return self._m_levels if hasattr(self, '_m_levels') else None

                        self._m_levels = (self.flags >> 12)
                        return self._m_levels if hasattr(self, '_m_levels') else None

                    @property
                    def ref_idx(self):
                        if hasattr(self, '_m_ref_idx'):
                            return self._m_ref_idx if hasattr(self, '_m_ref_idx') else None

                        self._m_ref_idx = (self.flags >> 8)
                        return self._m_ref_idx if hasattr(self, '_m_ref_idx') else None

                    @property
                    def has_attr(self):
                        if hasattr(self, '_m_has_attr'):
                            return self._m_has_attr if hasattr(self, '_m_has_attr') else None

                        self._m_has_attr = (self.flags & (1 << 9)) != 0
                        return self._m_has_attr if hasattr(self, '_m_has_attr') else None

                    @property
                    def has_tag(self):
                        if hasattr(self, '_m_has_tag'):
                            return self._m_has_tag if hasattr(self, '_m_has_tag') else None

                        self._m_has_tag = (self.flags & (1 << 10)) != 0
                        return self._m_has_tag if hasattr(self, '_m_has_tag') else None

                    @property
                    def type(self):
                        if hasattr(self, '_m_type'):
                            return self._m_type if hasattr(self, '_m_type') else None

                        self._m_type = (self.flags & 255)
                        return self._m_type if hasattr(self, '_m_type') else None

                    @property
                    def is_object(self):
                        if hasattr(self, '_m_is_object'):
                            return self._m_is_object if hasattr(self, '_m_is_object') else None

                        self._m_is_object = (self.flags & (1 << 8)) != 0
                        return self._m_is_object if hasattr(self, '_m_is_object') else None


                class New(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.type = self._io.read_u4le()
                        self.levels = self._io.read_u4le()
                        self.is_object = self._io.read_u4le()

                    def _read_be(self):
                        self.type = self._io.read_u4be()
                        self.levels = self._io.read_u4be()
                        self.is_object = self._io.read_u4be()


                @property
                def levels(self):
                    if hasattr(self, '_m_levels'):
                        return self._m_levels if hasattr(self, '_m_levels') else None

                    self._m_levels = (self.flagz_new.levels if self._root.is_new == 1 else self.flagz_old.levels)
                    return self._m_levels if hasattr(self, '_m_levels') else None

                @property
                def ref_idx(self):
                    if hasattr(self, '_m_ref_idx'):
                        return self._m_ref_idx if hasattr(self, '_m_ref_idx') else None

                    self._m_ref_idx = (0 if self._root.is_new == 1 else self.flagz_old.ref_idx)
                    return self._m_ref_idx if hasattr(self, '_m_ref_idx') else None

                @property
                def has_attr(self):
                    if hasattr(self, '_m_has_attr'):
                        return self._m_has_attr if hasattr(self, '_m_has_attr') else None

                    self._m_has_attr = (False if self._root.is_new == 1 else self.flagz_old.has_attr)
                    return self._m_has_attr if hasattr(self, '_m_has_attr') else None

                @property
                def has_tag(self):
                    if hasattr(self, '_m_has_tag'):
                        return self._m_has_tag if hasattr(self, '_m_has_tag') else None

                    self._m_has_tag = (True if self._root.is_new == 1 else self.flagz_old.has_tag)
                    return self._m_has_tag if hasattr(self, '_m_has_tag') else None

                @property
                def type(self):
                    if hasattr(self, '_m_type'):
                        return self._m_type if hasattr(self, '_m_type') else None

                    self._m_type = self._root.Body.Item.Type((self.flagz_new.type if self._root.is_new == 1 else self.flagz_old.type))
                    return self._m_type if hasattr(self, '_m_type') else None

                @property
                def is_object(self):
                    if hasattr(self, '_m_is_object'):
                        return self._m_is_object if hasattr(self, '_m_is_object') else None

                    self._m_is_object = (self.flagz_new.is_object != 0 if self._root.is_new == 1 else self.flagz_old.is_object)
                    return self._m_is_object if hasattr(self, '_m_is_object') else None


            class AltrepSxp(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.info = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.state = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.attr = self._root.Body.Item(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    self.info = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.state = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.attr = self._root.Body.Item(self._io, self, self._root, self._is_le)


            class DottedPair(KaitaiStruct):
                def __init__(self, flagz, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self.flagz = flagz
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    if self.flagz.has_attr:
                        self.attrib = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    if self.flagz.has_tag:
                        self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    self.car = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.cdr = self._root.Body.Item(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    if self.flagz.has_attr:
                        self.attrib = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    if self.flagz.has_tag:
                        self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    self.car = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.cdr = self._root.Body.Item(self._io, self, self._root, self._is_le)


            class Extptrsxp(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.ptr = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    self.ptr = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)


            class Raw(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = self._io.read_bytes(self.len.value)

                def _read_be(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = self._io.read_bytes(self.len.value)


            class VSizeLen(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len1 = self._io.read_s4le()
                    if self.len1 == -1:
                        self.len2 = self._io.read_s8le()


                def _read_be(self):
                    self.len1 = self._io.read_s4be()
                    if self.len1 == -1:
                        self.len2 = self._io.read_s8be()


                @property
                def value(self):
                    if hasattr(self, '_m_value'):
                        return self._m_value if hasattr(self, '_m_value') else None

                    self._m_value = (self.len1 if self.len1 != -1 else ((self.len1 << 32) + self.len2))
                    return self._m_value if hasattr(self, '_m_value') else None


            class RealVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_f8le()


                def _read_be(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_f8be()



            class RefIndex(KaitaiStruct):
                def __init__(self, index, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self.index = index
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    if self.index != 0:
                        self.ref_index_stored = self._io.read_s4le()


                def _read_be(self):
                    if self.index != 0:
                        self.ref_index_stored = self._io.read_s4be()


                @property
                def ref_index(self):
                    if hasattr(self, '_m_ref_index'):
                        return self._m_ref_index if hasattr(self, '_m_ref_index') else None

                    self._m_ref_index = (self.ref_index_stored if self.index != 0 else self.index)
                    return self._m_ref_index if hasattr(self, '_m_ref_index') else None


            class ComplexVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_f8le()


                def _read_be(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_f8be()



            class Specialsxp(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    _on = self._root.is_new
                    if _on == 0:
                        self.index = self._root.Body.PasStr(self._io, self, self._root, self._is_le)
                    elif _on == 1:
                        self.index = self._root.Body.Item.Specialsxp.MaxeltsizeStr(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    _on = self._root.is_new
                    if _on == 0:
                        self.index = self._root.Body.PasStr(self._io, self, self._root, self._is_le)
                    elif _on == 1:
                        self.index = self._root.Body.Item.Specialsxp.MaxeltsizeStr(self._io, self, self._root, self._is_le)

                class MaxeltsizeStr(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.value = (self._io.read_bytes((self._root.max_elt_size - 1))).decode(u"utf-8")

                    def _read_be(self):
                        self.value = (self._io.read_bytes((self._root.max_elt_size - 1))).decode(u"utf-8")



            class Bytecode(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.car = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.consts = self._root.Body.Item.Bytecode.Consts(self._io, self, self._root, self._is_le)

                def _read_be(self):
                    self.car = self._root.Body.Item(self._io, self, self._root, self._is_le)
                    self.consts = self._root.Body.Item.Bytecode.Consts(self._io, self, self._root, self._is_le)

                class Consts(KaitaiStruct):
                    def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                        self._io = _io
                        self._parent = _parent
                        self._root = _root if _root else self
                        self._is_le = _is_le
                        self._read()

                    def _read(self):

                        if self._is_le == True:
                            self._read_le()
                        elif self._is_le == False:
                            self._read_be()
                        else:
                            raise Exception("Unable to decide endianness")

                    def _read_le(self):
                        self.count = self._io.read_s4le()
                        self.consts = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)

                    def _read_be(self):
                        self.count = self._io.read_s4be()
                        self.consts = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)

                    class Constant(KaitaiStruct):
                        def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                            self._io = _io
                            self._parent = _parent
                            self._root = _root if _root else self
                            self._is_le = _is_le
                            self._read()

                        def _read(self):

                            if self._is_le == True:
                                self._read_le()
                            elif self._is_le == False:
                                self._read_be()
                            else:
                                raise Exception("Unable to decide endianness")

                        def _read_le(self):
                            self.type = self._root.Body.Item.Type(self._io.read_s4le())
                            _on = self.type
                            if _on == self._root.Body.Item.Type.langsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcrepref:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcrepEf(self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcrepdef:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcrepEf(self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.attrlistsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(1, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.attrlangsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(1, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.listsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcodesxp:
                                self.value = self._root.Body.Item.Bytecode(self._io, self, self._root, self._is_le)

                        def _read_be(self):
                            self.type = self._root.Body.Item.Type(self._io.read_s4be())
                            _on = self.type
                            if _on == self._root.Body.Item.Type.langsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcrepref:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcrepEf(self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcrepdef:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcrepEf(self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.attrlistsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(1, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.attrlangsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(1, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.listsxp:
                                self.value = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)
                            elif _on == self._root.Body.Item.Type.bcodesxp:
                                self.value = self._root.Body.Item.Bytecode(self._io, self, self._root, self._is_le)

                        class BcrepEf(KaitaiStruct):
                            def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                                self._io = _io
                                self._parent = _parent
                                self._root = _root if _root else self
                                self._is_le = _is_le
                                self._read()

                            def _read(self):

                                if self._is_le == True:
                                    self._read_le()
                                elif self._is_le == False:
                                    self._read_be()
                                else:
                                    raise Exception("Unable to decide endianness")

                            def _read_le(self):
                                self.pos = self._io.read_s4le()
                                self.type = self._io.read_s4le()
                                self.lang = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)

                            def _read_be(self):
                                self.pos = self._io.read_s4be()
                                self.type = self._io.read_s4be()
                                self.lang = self._root.Body.Item.Bytecode.Consts.Constant.BcLang(0, self._io, self, self._root, self._is_le)


                        class BcLang(KaitaiStruct):
                            def __init__(self, has_attr, _io, _parent=None, _root=None, _is_le=None):
                                self._io = _io
                                self._parent = _parent
                                self._root = _root if _root else self
                                self._is_le = _is_le
                                self.has_attr = has_attr
                                self._read()

                            def _read(self):

                                if self._is_le == True:
                                    self._read_le()
                                elif self._is_le == False:
                                    self._read_be()
                                else:
                                    raise Exception("Unable to decide endianness")

                            def _read_le(self):
                                if self.has_attr != 0:
                                    self.attr = self._root.Body.Item(self._io, self, self._root, self._is_le)

                                self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)
                                self.car = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)
                                self.cdr = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)

                            def _read_be(self):
                                if self.has_attr != 0:
                                    self.attr = self._root.Body.Item(self._io, self, self._root, self._is_le)

                                self.tag = self._root.Body.Item(self._io, self, self._root, self._is_le)
                                self.car = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)
                                self.cdr = self._root.Body.Item.Bytecode.Consts.Constant(self._io, self, self._root, self._is_le)





            class IntVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_s4le()


                def _read_be(self):
                    self.len = self._root.Body.Item.VSizeLen(self._io, self, self._root, self._is_le)
                    self.vec = [None] * (self.len.value)
                    for i in range(self.len.value):
                        self.vec[i] = self._io.read_s4be()



            class CharVec(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.len = self._io.read_s4le()
                    self.vec = (self._io.read_bytes(self.len)).decode(u"utf-8")

                def _read_be(self):
                    self.len = self._io.read_s4be()
                    self.vec = (self._io.read_bytes(self.len)).decode(u"utf-8")


            class Symsxp(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    if self._root.is_new == 0:
                        self.symsxp_old = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    if self._root.is_new == 1:
                        self.pos = self._io.read_u4le()


                def _read_be(self):
                    if self._root.is_new == 0:
                        self.symsxp_old = self._root.Body.Item(self._io, self, self._root, self._is_le)

                    if self._root.is_new == 1:
                        self.pos = self._io.read_u4be()


                @property
                def contents_new(self):
                    if hasattr(self, '_m_contents_new'):
                        return self._m_contents_new if hasattr(self, '_m_contents_new') else None

                    if False:
                        self._m_contents_new = self.symsxp_old

                    return self._m_contents_new if hasattr(self, '_m_contents_new') else None

                @property
                def contents(self):
                    if hasattr(self, '_m_contents'):
                        return self._m_contents if hasattr(self, '_m_contents') else None

                    self._m_contents = (self.contents_new if self._root.is_new == 1 else self.symsxp_old.contents)
                    return self._m_contents if hasattr(self, '_m_contents') else None


            class Complex(KaitaiStruct):
                def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                    self._io = _io
                    self._parent = _parent
                    self._root = _root if _root else self
                    self._is_le = _is_le
                    self._read()

                def _read(self):

                    if self._is_le == True:
                        self._read_le()
                    elif self._is_le == False:
                        self._read_be()
                    else:
                        raise Exception("Unable to decide endianness")

                def _read_le(self):
                    self.r = self._io.read_f8le()
                    self.i = self._io.read_f8le()

                def _read_be(self):
                    self.r = self._io.read_f8be()
                    self.i = self._io.read_f8be()



        class Version(KaitaiStruct):
            def __init__(self, _io, _parent=None, _root=None, _is_le=None):
                self._io = _io
                self._parent = _parent
                self._root = _root if _root else self
                self._is_le = _is_le
                self._read()

            def _read(self):

                if self._is_le == True:
                    self._read_le()
                elif self._is_le == False:
                    self._read_be()
                else:
                    raise Exception("Unable to decide endianness")

            def _read_le(self):
                self.packed = self._io.read_s4le()

            def _read_be(self):
                self.packed = self._io.read_s4be()

            @property
            def v(self):
                if hasattr(self, '_m_v'):
                    return self._m_v if hasattr(self, '_m_v') else None

                self._m_v = (self.packed >> 16)
                return self._m_v if hasattr(self, '_m_v') else None

            @property
            def p(self):
                if hasattr(self, '_m_p'):
                    return self._m_p if hasattr(self, '_m_p') else None

                self._m_p = ((self.packed & 65535) >> 8)
                return self._m_p if hasattr(self, '_m_p') else None

            @property
            def s(self):
                if hasattr(self, '_m_s'):
                    return self._m_s if hasattr(self, '_m_s') else None

                self._m_s = (self.packed & 255)
                return self._m_s if hasattr(self, '_m_s') else None



    @property
    def is_new(self):
        if hasattr(self, '_m_is_new'):
            return self._m_is_new if hasattr(self, '_m_is_new') else None

        self._m_is_new = 0
        return self._m_is_new if hasattr(self, '_m_is_new') else None

    @property
    def max_elt_size(self):
        if hasattr(self, '_m_max_elt_size'):
            return self._m_max_elt_size if hasattr(self, '_m_max_elt_size') else None

        self._m_max_elt_size = 8192
        return self._m_max_elt_size if hasattr(self, '_m_max_elt_size') else None


