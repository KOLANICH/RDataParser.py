from .r_data import *

try:
	import numpy
	arrayCtor=numpy.array
except ImportError:
	def arrayCtor(x, *args, **kwargs):
		return x

def processItemsVec(cts):
	res=[]
	for subEl in cts.vec:
		res.append(postprocess(subEl))
	return res

def processRawVec(cts):
	return arrayCtor(cts.vec)

class ListXP:
	__slots__=("car", "cdr", "tag")
	def __init__(self, tag, car, cdr):
		self.car=car
		self.cdr=cdr
		self.tag=tag

class postprocessors:
	def symsxp(cts):
		return cts.contents.vec
	
	def listsxp(cts):
		return ListXP(postprocess(cts.tag), postprocess(cts.car), postprocess(cts.cdr))
	
	vecsxp=processItemsVec
	strsxp=processItemsVec
	exprsxp=processItemsVec
	realsxp=processRawVec
	lglsxp=processRawVec
	intsxp=processRawVec
	
	def charsxp(cts):
		return cts.vec
	
	def nilvalue_sxp(cts):
		return None
	
	def cplxsxpel():
		return [num.r+num.i*1j for num in el.contents.vec]
	

def postprocess(el):
	if hasattr(el, "contents"):
		contents = el.contents
	else:
		contents = None
	return getattr(postprocessors, el.flags.type.name)(contents)

def parseRData(file="./prothro.rda"):
	a=RData.from_file(file)
	return postprocess(a.body.ref_table)
